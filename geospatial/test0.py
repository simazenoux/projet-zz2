## for data
import numpy as np
import pandas as pd
## for plotting
import matplotlib.pyplot as plt
import seaborn as sns
## for geospatial
import folium
import geopy
## for machine learning
from sklearn import preprocessing, cluster
import scipy
## for deep learning
import minisom

# remember GPS coordinates (lon,lat) are (x,y) !!
stations_Feyzin_coord = [('Depot', 4.852482, 45.665686),
                         ('Gare de Feyzin', 4.852482, 45.665686),
                         ('Raffinerie', 4.84624, 45.67346),
                         ('Alfred Nobel', 4.85065, 45.67216),
                         ('Louise Michel', 4.85395, 45.67572),
                         ('11 novembre',4.85011, 45.67052),
                         ('Hector Berlioz', 4.8519, 45.6671),
                         ('Thomas',4.84783, 45.66326),
                         ('Les Razes', 4.84637, 45.66076),
                         ('Vernay', 4.85446, 45.68117),
                         ('Ile de la Chevre', 4.83489, 45.66682)]


#def mapStationNameCoord():
# mapCoord = {}
# for st in stations_Feyzin_coord:
#     name = st[0]; lon = st[1]; lat = st[2]
#     mapCoord[name] = (lon, lat)
   # return mapCoord
   

#Figure 4 of the paper
    # m = folium.Map(location=[45.6933, 4.8742], tiles='stamenterrain') #'cartodbpositron'
    # for st in stations_Feyzin_coord:
    #     name = st[0]; lat = st[2]; lon = st[1]
    #     if name != 'Depot':
    #         if name == 'Gare de Feyzin': col = 'red'
    #         else: col = 'blue'
    #         folium.CircleMarker(location=[lat, lon], popup=name, radius=5, color=col,
    #                            fill=True, fill_opacity=0.8, fill_color=col).add_to(m)
    #         folium.Marker(location=[lat, lon]).add_to(m)
    # m.save('loca.html')
    
    #**************************************************************************************#
    
#clustering avec SOM 

dtf=pd.read_csv("classeur1.csv")
X = dtf[["Latitude","Longitude"]]
map_shape = (2,2)  #nb d'elements defini le nb de cluster

## scale data
scaler = preprocessing.StandardScaler()
X_preprocessed = scaler.fit_transform(X.values)


## clustering
model = minisom.MiniSom(x=map_shape[0], y=map_shape[1], 
                        input_len=X.shape[1])
model.train_batch(X_preprocessed, num_iteration=100, verbose=False)


## build output dataframe
dtf_X = X.copy()
dtf_X["cluster"] = np.ravel_multi_index(np.array(
      [model.winner(x) for x in X_preprocessed]).T, dims=map_shape)


## find real centroids
cluster_centers = np.array([vec for center in model.get_weights() 
                            for vec in center])
closest, distances = scipy.cluster.vq.vq(cluster_centers, 
                                          X_preprocessed)
dtf_X["centroids"] = 0
for i in closest:
    dtf_X["centroids"].iloc[i] = 1
    
    
## add clustering info to the original dataset
dtf[["cluster","centroids"]] = dtf_X[["cluster","centroids"]]


## plot
k = dtf["cluster"].nunique()
fig, ax = plt.subplots()
sns.scatterplot(x="Latitude", y="Longitude", data=dtf, 
                palette=sns.color_palette("bright",k),
                hue='cluster', size="centroids", size_order=[1,0],
                legend="brief", ax=ax).set_title('Clustering')
th_centroids = scaler.inverse_transform(cluster_centers)
ax.scatter(th_centroids[:,0], th_centroids[:,1], s=50, c='black', 
            marker="x")


#************************************************************************#

#affichage de la solution , les clusters et leurs centroids


x, y = "Latitude", "Longitude"
color = "cluster"
marker = "centroids"
data = dtf.copy()
## create color column
lst_elements = sorted(list(dtf[color].unique()))
lst_colors = ['#%06X' % np.random.randint(0, 0xFFFFFF) for i in 
              range(len(lst_elements))]
data["color"] = data[color].apply(lambda x: 
                lst_colors[lst_elements.index(x)])

## create color column
lst_elements = sorted(list(dtf[color].unique()))
lst_colors = ['#%06X' % np.random.randint(0, 0xFFFFFF) for i in 
              range(len(lst_elements))]
data["color"] = data[color].apply(lambda x: 
                lst_colors[lst_elements.index(x)])
## create size column (scaled)
scaler = preprocessing.MinMaxScaler(feature_range=(3,15))

## initialize the map with the starting location
m = folium.Map(location=[45.6933, 4.8742], tiles='stamenterrain',zoom_start=13)
# add points

for i in range(0,11):
    folium.CircleMarker(
    location=[data['Longitude'][i],data['Latitude'][i]],color=data['color'][i],fill=True,radius=10).add_to(m)
    


## add html legend
legend_html = """<div style="position:fixed; bottom:10px; left:10px; border:2px solid black; z-index:9999; font-size:14px;">&nbsp;<b>"""+color+""":</b><br>"""
for i in lst_elements:
     legend_html = legend_html+"""&nbsp;<i class="fa fa-circle 
     fa-1x" style="color:"""+lst_colors[lst_elements.index(i)]+"""">
     </i>&nbsp;"""+str(i)+"""<br>"""
legend_html = legend_html+"""</div>"""
m.get_root().html.add_child(folium.Element(legend_html))

## add centroids marker
lst_elements = sorted(list(dtf[marker].unique()))
for i in range(0,11):
    if(data['centroids'][i]==1):
        folium.Marker(
        location=[data['Longitude'][i],data['Latitude'][i]],icon=folium.Icon(color="black")).add_to(m)
   

## plot the map
m.save("local.html")

#associer des pickups et delivery appartenant au memes clusters






