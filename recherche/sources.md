# Sources

Learning Deep Représentations for Graph Clustering:
<https://ojs.aaai.org/index.php/AAAI/article/view/8916>

Sparse, Stacked and Variational Autoencoder:
<https://medium.com/@venkatakrishna.jonnalagadda/sparse-stacked-and-variational-autoencoder-efe5bfe73b64>
