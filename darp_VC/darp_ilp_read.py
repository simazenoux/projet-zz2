from darp_ilp_common import *

#
# IO methods
#

def readTimeGuy(filename):
    stationName = cost_tmp = nbVertices = None
    csv_reader = csv.reader(open(filename, mode='r'), delimiter=';')
    line_count = 0
    for line in csv_reader:
        if line_count == 0:
            #init nbVertices and stationName
            nbVertices = len(line)
            stationName = []
            for idx in range(nbVertices): stationName.append(line[idx])
            cost_tmp = np.zeros((nbVertices, nbVertices, 1), dtype=int)
            line_count += 1
            continue
        for idx in range(nbVertices):
            cost_tmp[line_count-1, idx, 0] = float(line[idx]) #seconds
        line_count += 1
    return cost_tmp, stationName, nbVertices



def readDistanceGuy(filename, nbVertices):
    csv_reader = csv.reader(open(filename, mode='r'), delimiter=';')
    line_count = 0
    for line in csv_reader:
        if line_count == 0:
            cost_tmp = np.zeros((nbVertices, nbVertices, 1), dtype=float)
            line_count += 1
            continue
        for idx in range(nbVertices):
            cost_tmp[line_count-1, idx, 0] = float(line[idx])*1000 #meters
        line_count += 1
    return cost_tmp



def readDemandGuy(filename, nbVertices):
    csv_reader = csv.reader(open(filename, mode='r'), delimiter=';')
    line_count = 0
    for line in csv_reader:
        if line_count == 0:
            demande = np.zeros((nbVertices, nbVertices), dtype=int)
            line_count += 1
            continue
        for idx in range(nbVertices):
            demande[line_count-1, idx] = math.ceil(float(line[idx])) #number of ppl
        line_count += 1
    return demande


def GHG_VP(demande, distance, nbVertices):
    return sum([distance[i, j]*demande[i, j]*175/(1.33*1000*1000) for i in range(1, nbVertices)
                for j in range(1, nbVertices) if i != j])


def readDataGuy(filenameTime, filenameDistance, filenameDemande, durationStop):
    cost_tmp_t, stationName, nbVertices = readTimeGuy(filenameTime)
    cost_tmp_d = readDistanceGuy(filenameDistance, nbVertices)
    demand = readDemandGuy(filenameDemande, nbVertices)

    #1. Split nodes into pickup and deliveries
    #oldVertices -> to keep tack of old nodes before splitting
    #request -> to keep track of postive (pickup) and negative (delivery) demands
    V=[]; P=[]; D=[]; Vname=[stationName[0]]

    #starting depot
    count_vertices = 0
    V.append(count_vertices); count_vertices +=1
    initVertices = [0]
    request = [0]

    for idx_row in range(1, nbVertices):
        for idx_col in range(1, nbVertices):
            d = demand[idx_row, idx_col]
            if idx_row == idx_col: d = 0
            if d > 0:
                #pickup point
                Vname.append(stationName[idx_row])
                V.append(count_vertices)
                P.append(count_vertices)
                count_vertices += 1
                initVertices.append(idx_row)
                request.append(d*1)

                #delivery point
                Vname.append(stationName[idx_col])
                V.append(count_vertices)
                D.append(count_vertices)
                count_vertices += 1
                initVertices.append(idx_col)
                request.append(d*-1)
    #end depot
    V.append(count_vertices)
    Vname.append(stationName[0])
    count_vertices += 1
    initVertices.append(0)
    request.append(0)

    #print("GHG_VP ",GHG_VP(demande, cost_tmp_d, nbVertices))

    #2. Create cost matrices
    cost_time = np.zeros((count_vertices, count_vertices, 1), dtype=int)
    cost_distance = np.zeros((count_vertices, count_vertices), dtype=int)
    for idx_row in range(count_vertices):
        for idx_col in range(count_vertices):
            cost_time[idx_row, idx_col, 0] = cost_tmp_t[initVertices[idx_row], initVertices[idx_col]]
            cost_distance[idx_row, idx_col] = cost_tmp_d[initVertices[idx_row], initVertices[idx_col]]
            if cost_time[idx_row, idx_col, 0] > 1000:
                print("... ", idx_row, " ", idx_col)
            #print(idx_row, idx_col, prevVertices[idx_row],prevVertices[idx_col], cost_time[idx_row, idx_col, 0])
    #costs to infinity going to origin depot or coming from end depot
    for idx in range(count_vertices):
        cost_time[idx, 0, 0] = INFTY
        cost_distance[idx, 0] = INFTY
        cost_time[count_vertices-1, idx, 0] = INFTY
        cost_distance[count_vertices-1, idx] = INFTY
  

    #2. Create stop durations
    duration_stop = np.zeros(count_vertices, dtype=int)
    for idx in range(count_vertices):
        duration_stop[idx] = durationStop
    duration_stop[0] = duration_stop[count_vertices-1] = 0

    return ContextOptim(V=V, P=P, D=D, request=request, nbVertices=count_vertices,
                       nbVehicles=None, nbTimeSteps=1, capacity=None, max_riding= None,
                       horizon = None, cost_time=cost_time,  cost_distance=cost_distance,
                       duration_stop=duration_stop, weightsObj=None, emissionGHG=None,
                       Vname=Vname)
