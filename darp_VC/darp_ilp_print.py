from darp_ilp_common import *


#
# draw + print + show
#

# remember GPS coordinates (lon,lat) are (x,y) !!
stations_Feyzin_coord = [('Depot', 4.852482, 45.665686),
                         ('Gare de Feyzin', 4.852482, 45.665686),
                         ('Raffinerie', 4.84624, 45.67346),
                         ('Alfred Nobel', 4.85065, 45.67216),
                         ('Louise Michel', 4.85395, 45.67572),
                         ('11 novembre',4.85011, 45.67052),
                         ('Hector Berlioz', 4.8519, 45.6671),
                         ('Thomas',4.84783, 45.66326),
                         ('Les Razes', 4.84637, 45.66076),
                         ('Vernay', 4.85446, 45.68117),
                         ('Ile de la Chevre', 4.83489, 45.66682)]


def mapStationNameCoord():
    mapCoord = {}
    for st in stations_Feyzin_coord:
        name = st[0]; lon = st[1]; lat = st[2]
        mapCoord[name] = (lon, lat)
    return mapCoord


def plotMapwithStations():
    #Figure 4 of the paper
    m = folium.Map(location=[45.6933, 4.8742], tiles='stamenterrain') #'cartodbpositron'
    for st in stations_Feyzin_coord:
        name = st[0]; lat = st[2]; lon = st[1]
        if name != 'Depot':
            if name == 'Gare de Feyzin': col = 'red'
            else: col = 'blue'
            folium.CircleMarker(location=[lat, lon], popup=name, radius=5, color=col,
                               fill=True, fill_opacity=0.8, fill_color=col).add_to(m)
            folium.Marker(location=[lat, lon], icon=DivIcon(icon_size=(130, 35), icon_anchor=(0,0),
        html='<div style="font-size: 12pt; color : {}"><b>{}</b></div>'.format(col, name))).add_to(m)
    m.save('run/map.html')


def plotRoutes(method, x, ctx):
    north, south, east, west = 45.6933, 45.6433, 4.8742, 4.8268
    G = ox.graph_from_bbox(north, south, east, west) #,network_type='drive'
    drawRoutes(G, method, constructRoutes(x, ctx), ctx)


def constructRoutes(x, ctx):
    V = ctx.getV()
    n = ctx.getNbVertices()
    Vname = ctx.getVname()
    Arcs = forbiddenArcs(V, ctx.getP(), ctx.getD(), n, True, ctx)

    mapCoord = mapStationNameCoord()
    routes_names = []
    routes_coord = []
    for k in ctx.getK():
        current = 0
        route_k_names = []
        route_k_coord = []
        while current != endDepot(n):
            bool_break = True
            for i in V:
                if bool_break:
                    if (current, i) not in Arcs:
                        if x[current, i, k].solution_value == 1:
                            if len(Vname[i]) != 2: #not a couple
                                route_k_names.append(Vname[i])
                                coord = mapCoord[Vname[i]]
                                route_k_coord.append(coord)
                            else:
                                route_k_names.append(Vname[i][0])
                                coord = mapCoord[Vname[i][0]]
                                route_k_coord.append(coord)

                                route_k_names.append(Vname[i][1])
                                coord = mapCoord[Vname[i][1]]
                                route_k_coord.append(coord)
                            current = i
                            bool_break = False
        routes_names.append(route_k_names)
        routes_coord.append(route_k_coord)
    return routes_coord





def searchNodeOSMNX(G, coord):
    lon_target = coord[0]
    lat_target = coord[1]
    dist_best = INFTY
    for idx in G.nodes:
        lon = G.nodes[idx]['x']
        lat = G.nodes[idx]['y']
        dist = math.sqrt(pow(lon-lon_target,2)+pow(lat-lat_target,2))
        if dist < dist_best:
            best = G.nodes[idx]
            dist_best = dist
    return best['osmid']




def drawRoutes(G, method, routes, ctx):
    col_base = ['blue', 'red', 'green', 'orange', 'purple', 'c', 'm', 'yellow', 'k']
    mapCoord = mapStationNameCoord()
    for k in ctx.getK():
        tracks = []; route_colors = []; node_colors = []
        #print(routes[k])
        orig = mapCoord['Depot']
        for dst in routes[k]:
            if dst != startDepot():
                orig_net = searchNodeOSMNX(G, orig)
                dest_net = searchNodeOSMNX(G, dst)
                #orig = ox.get_nearest_node(self.G, self.geoInfo.getCoord(arc[0]), method='euclidean')
                segments = nx.shortest_path(G, orig_net, dest_net, weight='length', method='dijkstra')
                tracks.append(segments)

                col_segments = col_base[k % len(col_base)]
                node_colors.extend([col_segments]*2)
                route_colors.extend([col_segments])
        fig, ax = ox.plot_graph_routes(G, tracks, route_colors=col_base[k], bgcolor='w',
                                       node_size=0, show=False, close=False) #save=True
        for st in stations_Feyzin_coord:
            name = st[0]; lon = st[1]; lat = st[2]
            if name != 'Depot':
                ax.annotate(name, (lon, lat), size=11)
        fig.savefig("run/routes_v"+str(k+1)+"_"+method+".png")
