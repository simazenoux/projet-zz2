from darp_ilp_common import *
from darp_ilp_contract import *

#
# Node to Vec Operations
#

def similarity_N2V(G_init, nbVertices):
    #run the model of Node2Vec
    # node2vec = Node2Vec(G_init, dimensions=20, walk_length=16,
    #                     num_walks=100, p=0.5, q=1) #
    node2vec = Node2Vec(G_init, dimensions=30, walk_length=6,
                        num_walks=1000, p=0.5, q=1, workers=10, quiet=True) #
    model_n2v = node2vec.fit(window=10, min_count=1)
    nx.write_multiline_adjlist(G_init, "run/G_init.graph")
    model_n2v.wv.save_word2vec_format("run/embedding.emb")
    model_n2v.save("run/embedding.model")

    # or just load the model ...
    #G_init = nx.read_multiline_adjlist("run/G_init.graph")
    # model_n2v = Word2Vec.load("run/embedding.model")
    # model_n2v.wv.load_word2vec_format("run/embedding.emb")

    #similarity matrix ... compute the similarity between each couple of nodes
    similarity_matrix = np.zeros((nbVertices, nbVertices), dtype=float)
    for idx_row in G_init.nodes:
        list_simi = model_n2v.wv.most_similar(str(idx_row), topn=nbVertices)
        #list_simi is in the form of [(node, similarity)()()..]
        for e in range(len(list_simi)):
            idx_col = int(list_simi[e][0])
            measure = float(list_simi[e][1])
            similarity_matrix[int(idx_row), idx_col] = measure
    #sorted_measure_increasing= np.sort(similarity_matrix, axis=None)
    #sorted_measure = sorted_measure_increasing[::-1]
    return similarity_matrix

def getCouplePandD_N2V(P, similarity_matrix):
    couplePandD = {}
    for i in P:
        for j in P:
            if i != j:
                measure = similarity_matrix[i, j] + similarity_matrix[delivery(i), delivery(j)] + \
                          similarity_matrix[i, delivery(i)] + similarity_matrix[j, delivery(j)] + \
                          similarity_matrix[i, delivery(j)] + similarity_matrix[delivery(i), j]
                measure += similarity_matrix[j, i] + similarity_matrix[delivery(j), delivery(i)] + \
                          similarity_matrix[delivery(i), i] + similarity_matrix[delivery(j), j] + \
                          similarity_matrix[delivery(j), i] + similarity_matrix[j, delivery(i)]
                measure = measure / 12.0
                couplePandD[(i, j)] = measure
                couplePandD[(j, i)] = measure

    sorted_list = sorted(couplePandD.items(), key=operator.itemgetter(1), reverse=True)
    #sorted_list2 = sorted_list[0:int(nbReduction)]
    return sorted_list



def contractN2V(G_init, ctx_init, rContra, durationStop, cap, maxR, times, logging):
    if logging: print("\n\nN2V")
    V = ctx_init.getV()
    P = ctx_init.getP()
    D = ctx_init.getD()
    Vname = ctx_init.getVname()
    nbVertices = ctx_init.getNbVertices()
    request = ctx_init.getRequest()
    cost_time = ctx_init.getCost_time()
    cost_distance = ctx_init.getCost_distance()

    #1. Reduction
    nbReduction = int((nbVertices-2)*rContra)
    if nbReduction % 2 ==1: nbReduction = nbReduction -1

    total_cost_best = total_cost_inside_best = INFTY
    nodeToReplace_best = {}
    embeddedOrder_best = {}
    count_reduced_best = None
    for t in range(times):
        similarity_matrix = similarity_N2V(G_init, nbVertices)
        sorted_list = getCouplePandD_N2V(P, similarity_matrix)
        #print("coucou ", len(sorted_list), sorted_list)
        nodeToReplace, embeddedOrder, total_cost, total_cost_inside, count_reduced = \
            orderCouplePandD(nbReduction, sorted_list, cost_time,request, maxR, cap, Vname, logging)
        #print("cost reduction (N2V)", total_cost, "node reduced", count_reduced, "\n")
        if total_cost < total_cost_best and count_reduced == nbReduction:
            nodeToReplace_best = nodeToReplace
            embeddedOrder_best = embeddedOrder
            total_cost_best = total_cost
            total_cost_inside_best = total_cost_inside
            count_reduced_best = count_reduced
    #print("cost reduction (N2V)", total_cost_best, "node reduced", count_reduced_best, "\n") #nodeToReplace_best



    #2. construct a new context for optimization
    V_new, P_new, D_new, request_new, nbVertices_new, maptoVinit = \
        setNewV(nodeToReplace_best, embeddedOrder_best, V, P, D, request, nbVertices)
    cost_time_new, cost_distance_new, duration_stop_new = \
        setNewCosts(cost_time, cost_distance, durationStop, maptoVinit, nbVertices_new)
    request_new = request_new[0:int(nbVertices_new)]
    Vname_new = setNewVname(V_new, ctx_init.getVname(), maptoVinit)
    #print("coco", len(maptoVinit), maptoVinit)



    return ContextOptim(V=V_new, P=P_new, D=D_new,
                       request=request_new,
                       nbVertices=nbVertices_new,
                       nbVehicles=ctx_init.getNbVehicles(),
                       nbTimeSteps=1,
                       capacity=cap,
                       max_riding= maxR,
                       horizon=ctx_init.getHorizon(),
                       cost_time=cost_time_new,
                       cost_distance=cost_distance_new,
                       duration_stop=duration_stop_new,
                       weightsObj=ctx_init.getWeightsObj(),
                       emissionGHG=ctx_init.getEmissionGHG(),
                       Vname=Vname_new), maptoVinit




# for idx_row in range(nbVertices_new):
#        for idx_col in range(nbVertices_new):
#            prev_row = mappingVinit[idx_row]
#            prev_col = mappingVinit[idx_col]
#            # if not isinstance(prev_col, int): print("prev_col ", prev_col, prev_col[0], prev_col[1])
#            # if not isinstance(prev_row, int): print("prev_row ", prev_row, prev_row[0], prev_row[1])
#            if isinstance(prev_row, int) and isinstance(prev_col, int):
#                cost_time_new[idx_row, idx_col, 0] = cost_time[prev_row, prev_col, 0]
#                cost_distance_new[idx_row, idx_col] = cost_distance[prev_row, prev_col]
#            elif isinstance(prev_row, int) and not isinstance(prev_col, int):
#                if idx_col in P:
#                    cost_time_new[idx_row, idx_col, 0] = cost_time[prev_row, prev_col[0], 0]
#                    cost_distance_new[idx_row, idx_col] = cost_distance[prev_row, prev_col[0]]
#                if idx_col in D:
#                    cost_time_new[idx_row, idx_col, 0] = cost_time[prev_row, prev_col[0], 0] \
#                                                         + cost_time[prev_col[0], prev_col[1], 0]
#                    cost_distance_new[idx_row, idx_col] = cost_distance[prev_row, prev_col[0]] \
#                                                          + cost_distance[prev_col[0], prev_col[1]]
#            elif not isinstance(prev_row, int) and isinstance(prev_col, int):
#                if idx_row in P:
#                    cost_time_new[idx_row, idx_col, 0] = cost_time[prev_row[0], prev_row[1], 0] + \
#                                                         cost_time[prev_row[1], prev_col, 0]
#                    cost_distance_new[idx_row, idx_col] = cost_distance[prev_row[0], prev_row[1]] + \
#                                                          cost_distance[prev_row[1], prev_col]
#                if idx_row in D:
#                    cost_time_new[idx_row, idx_col, 0] = cost_time[prev_row[1], prev_col, 0]
#                    cost_distance_new[idx_row, idx_col] = cost_distance[prev_row[1], prev_col]
#            elif not isinstance(prev_row, int) and not isinstance(prev_col, int):
#                if idx_row in P:
#                    if idx_col in P:
#                        cost_time_new[idx_row, idx_col, 0] = cost_time[prev_row[0], prev_row[1], 0] + \
#                                                             cost_time[prev_row[1], prev_col[0], 0]
#                        cost_distance_new[idx_row, idx_col] = cost_distance[prev_row[0], prev_row[1]] + \
#                                                              cost_distance[prev_row[1], prev_col[0]]
#                    if idx_col in D:
#                        cost_time_new[idx_row, idx_col, 0] = cost_time[prev_row[0], prev_row[1], 0] + \
#                                                             cost_time[prev_row[1], prev_col[0], 0] + \
#                                                             cost_time[prev_col[0], prev_col[1], 0]
#                        cost_distance_new[idx_row, idx_col] = cost_distance[prev_row[0], prev_row[1]] + \
#                                                              cost_distance[prev_row[1], prev_col[0]] + \
#                                                              cost_distance[prev_col[0], prev_col[1]]
#                if idx_row in D:
#                    if idx_col in P:
#                        cost_time_new[idx_row, idx_col, 0] = cost_time[prev_row[1], prev_col[0]]
#                        cost_distance_new[idx_row, idx_col] = cost_distance[prev_row[1], prev_col[0]]
#
#                    if idx_col in D:
#                        cost_time_new[idx_row, idx_col, 0] = cost_time[prev_row[1], prev_col[0], 0] + \
#                                                             cost_time[prev_col[0], prev_col[1], 0]
#                        cost_distance_new[idx_row, idx_col] = cost_distance[prev_row[1], prev_col[0]] + \
#                                                              cost_distance[prev_col[0], prev_col[1]]
#

