import csv, math, operator, time, sys, random, io
import numpy as np
import networkx as nx
import pandas as pd
import statistics as stat
from node2vec import Node2Vec
from gensim.models import Word2Vec
from docplex.mp.model import Model
from k_means_constrained import KMeansConstrained


import folium
from folium.features import DivIcon
import osmnx as ox
import matplotlib.pyplot as plt
if 'matplotlib' not in sys.modules:
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import osmnx as ox





INFTY = sys.maxsize / 30000000000 #9223372036854775807


def startDepot(): return 0
def endDepot(nbVertices): return nbVertices-1
def pickup(i): return i-1
def delivery(i): return i+1

def heuristicNbVehicle(): return


def forbiddenArcs(V, P, D, n, constaints, ctx):
    Q = ctx.getCapacity()
    MAX_RIDING = ctx.getMaxRiding()
    request = ctx.getRequest()
    cost_time = ctx.getCost_time()
    duration_stop = ctx.getDuration_stop()

    #relaxed
    # Arcs = [(i, j) for i in V for j in V if (j == startDepot()) or (i == endDepot(n))] + \
    #    [(i, i) for i in V] + [(startDepot(), endDepot(n))]
    Arcs = [(i, j) for i in V for j in V if (j == startDepot()) or (i == endDepot(n))] + \
           [(i, i) for i in V] + [(startDepot(), endDepot(n))] + \
           [(startDepot(), i) for i in D] + [(i, endDepot(n)) for i in P] + \
           [(delivery(i), i) for i in P] + [(i, pickup(i)) for i in D]

    count = count0 = 0
    for i in V:
        for j in V:
            count0 += 1
    if constaints:

        for i in P:
            for j in P+D:
                if (i, j) not in Arcs:
                    #max capacity
                    if request[i]+request[j] > Q:
                        Arcs += [(i, j)]
                        count += 1
                    #riding time
                    if cost_time[i, j, 0] + duration_stop[j] + \
                            cost_time[j, delivery(i), 0] > MAX_RIDING:
                        Arcs += [(i, j)]
                        Arcs += [(j, delivery(i))]
                        count += 2
        #time windows
        for i in V:
            for j in V:
                if (i, j) not in Arcs:
                    #Past
                    if i in P+D and j in P+D and \
                            ctx.getTw_start()[i] >= ctx.getTw_end()[j]:
                        Arcs += [(i, j)]
                        count += 1
                    if i in P and cost_time[i, j, 0] + duration_stop[j] + \
                            cost_time[j, delivery(i), 0] > ctx.getTw_end()[delivery(i)]:
                        Arcs += [(i, j)]
                        Arcs += [(j, delivery(i))]
                        count += 2
                    #Future .. last node if the current TW to the next TW is is D
                    if i in P and ctx.getTw_start()[j] > ctx.getTw_start()[i]:
                        Arcs += [(i, j)]
                        count += 1
                    if i in D and j in D and ctx.getTw_start()[j] > ctx.getTw_start()[i]:
                        Arcs += [(i, j)]
                        count += 1
        Arcs = list(dict.fromkeys(Arcs))
        # print("coco1 ", len(Arcs),count0, len(Arcs)/count0)
    return Arcs




class ContextOptim:
    def __init__(self):
        self.V = [] #sets of vertices .. It has a special structure :
        # startDepot pickup depot p d p d ... endDepot
        self.P = [] #sets of pickups
        self.D = [] #sets of deliveries
        self.K = [] #sets of vehicules
        self.tw_start = []
        self.tw_end = []
        self.request = []
        self.nbVertices = None
        self.nbVehicles = None
        self.nbTimeSteps = None
        self.capacity_vehicle = None
        self.horizon = None
        self.cost_time = None
        self.cost_distance = None
        self.duration_stop = None
        self.weightsObj = []
        self.emissionGHG = []

    def __init__(self, V, P, D, request, nbVertices, nbVehicles, nbTimeSteps,
                 capacity, max_riding, horizon, cost_time, cost_distance, duration_stop,
                 weightsObj, emissionGHG, Vname):
        self.V = V
        self.P = P
        self.D = D
        self.K = None
        self.tw_start = []
        self.tw_end = []
        self.request = request
        self.nbVertices = nbVertices
        self.nbVehicles = nbVehicles
        if isinstance(nbVehicles, int): self.K = range(nbVehicles)
        self.nbTimeSteps = nbTimeSteps
        self.capacity_vehicle = capacity
        self.max_riding = max_riding
        self.horizon = horizon
        self.cost_time = cost_time
        self.cost_distance = cost_distance
        self.duration_stop = duration_stop
        self.weightsObj = weightsObj
        self.emissionGHG = emissionGHG
        self.Vname = Vname

    def getV(self): return self.V
    def getP(self): return self.P
    def getD(self): return self.D
    def getK(self): return self.K
    def getTw_start(self): return self.tw_start
    def getTw_end(self): return self.tw_end
    def getRequest(self): return self.request
    def getNbVertices(self): return self.nbVertices
    def getNbVehicles(self): return self.nbVehicles
    def getNbTimeSteps(self): return self.nbTimeSteps
    def getCapacity(self): return self.capacity_vehicle
    def getMaxRiding(self): return self.max_riding
    def getHorizon(self): return self.horizon
    def getCost_time(self): return self.cost_time
    def getCost_distance(self): return self.cost_distance
    def getDuration_stop(self): return self.duration_stop
    def getWeightsObj(self): return self.weightsObj
    def getEmissionGHG(self): return self.emissionGHG
    def getVname(self): return self.Vname


    def setV(self, V):  self.V = V
    def setP(self, P):  self.P = P
    def setD(self, D):  self.D = D
    def setK(self, K):  self.K = K
    def setTw_start(self, tw_start): self.tw_start = tw_start
    def setTw_end(self, tw_end): self.tw_end = tw_end
    def setRequest(self, request):  self.request = request
    def setNbVertices(self, nbVertices):  self.nbVertices = nbVertices
    def setNbVehicles(self, nbVehicles): self.nbVehicles = nbVehicles; self.K = range(nbVehicles)
    def setNbTimeSteps(self, nbTimeSteps):  self.nbTimeSteps = nbTimeSteps
    def setCapacity(self, capacity_vehicle): self.capacity_vehicle = capacity_vehicle
    def setMaxRiding(self, max_riding): self.max_riding = max_riding
    def setHorizon(self, horizon): self.horizon = horizon
    def setCost_time(self, cost_time):  self.cost_time = cost_time
    def setCost_distance(self, cost_distance):  self.cost_distance = cost_distance
    def setDuration_stop(self, duration_stop):  self.duration_stop = duration_stop
    def setWeightsObj(self, weightsObj): self.weightsObj = weightsObj
    def setEmissionGHG(self, emissionGHG): self.emissionGHG = emissionGHG
    def setVname(self, Vname): self.Vname = Vname

    def print(self):
        print("V: ", self.V)
        print("P: ", self.P)
        print("D: ", self.D)
        print("K: ", self.K)
        print("capacity: ", self.capacity_vehicle)
        print("max_riding: ", self.max_riding)
        print("horizon: ", self.horizon)
        print("emission GHG: ", self.emissionGHG)
        print("request: ", self.request)
        print("duration_stop: ", self.duration_stop)
        print("cost_time: ")
        [print("(", i, ",", j, ") ", self.cost_time[i, j,0]) for i in self.V for j in self.V]





# Notes
# #bibliotheaue CPLEX
# c = cplex.Cplex()
# c.set_problem_name("darp")
# c.objective.set_sense(c.objective.sense.minimize)
# x = ["x_{i},{j}^{k}".format(i=i, j=j, k=k) for k in range(nbVehicles) \
#      for i in range(nbVertices) for j in range(nbVertices)]
# c.variables.add(obj=cost*nbVehicles, types=c.variables.type.binary, names=x)
# c.multiobj.set_num(2)
# c.multiobj.set_weight(1, 1) #-1 will mean maximum
# c.objective.set_name(0, "cost")
# #c.multiobj.set_linear(0, )
# c.objective.set_name(1, "satisfaction")
# c.multiobj.set_linear(1, ...)
# c.linear_constraints.add()
# c.write('darp_py.lp')
# #c.parameters.mip.pool.relgap.set(0.1) #obtain solution within ..% from optimal
# #c.parameters.dettimelimit.set(60000) #the global deterministic time limit
# try:
#     c.solve()
# except CplexSolverError:
#     print("Exception raised during solve")
#     return
#
#
#
#     ##google distance
#     gKey = "AIzaSyCFqBwV9siKc-jL49bDdP4oyFXOhbsXyvs"
#     gmaps = googlemaps.Client(key=gKey)
