from darp_ilp_common import *
from darp_ilp_read import *
from darp_ilp_node2v import *
from darp_ilp_clustering import *
from darp_ilp_basicReduction import *
from darp_ilp_print import *
from darp_ilp_requestReduction import *




def graphRouting(ctx):
    G = nx.Graph()
    V = ctx.getV()
    P = ctx.getP()
    D = ctx.getD()
    n = ctx.getNbVertices()
    cost_time = ctx.getCost_time()
    Arcs = forbiddenArcs(V, P, D, n, constaints=False, ctx=ctx)

    for i in V:
        for j in V:
            if (i, j) not in Arcs:
                G.add_edge(i, j, weight=cost_time[i, j, 0], attr='time')

    type= {} #add attribute of the type of node
    for i in V:
        if i == startDepot(): type[i] = 'startDepot'
        if i == endDepot(n): type[i] = 'endDepot'
        if i in P: type[i] = 'p'
        if i in D: type[i] = 'd'
    nx.set_node_attributes(G, type, 'type')
    return G


def generateTW(nTW, lengthTW, ctx):
    n = ctx.getNbVertices()
    tw_start = [0] * n
    tw_end = [0] * n

    P = ctx.getP()
    random.shuffle(P)
    size = int(len(P)/nTW)
    PP = [P[x::size] for x in range(size)]
    start=0
    for i in range(len(PP)):
        for j in PP[i]:
            tw_start[j] = start
            tw_start[delivery(j)] = start

            tw_end[j] = start + lengthTW
            tw_end[delivery(j)] = start + lengthTW
        start += lengthTW
    return tw_start, tw_end


def setTW(nTW, lengthTW, ctx_contacted, ctx_contacted1, ctx_contacted2, ctx_contacted3):
    start, end = generateTW(nTW=nTW, lengthTW=lengthTW, ctx=ctx_contacted)
    ctx_contacted.setTw_start(start)
    ctx_contacted.setTw_end(end)
    ctx_contacted1.setTw_start(start)
    ctx_contacted1.setTw_end(end)
    ctx_contacted2.setTw_start(start)
    ctx_contacted2.setTw_end(end)
    ctx_contacted3.setTw_start(start)
    ctx_contacted3.setTw_end(end)


def setStartSol(x, x_warm, model, useTW, ctx):
    V = ctx.getV()
    P = ctx.getP()
    D = ctx.getD()
    K = ctx.getK()
    n = len(V)
    Arcs = forbiddenArcs(V, P, D, n, constaints=True, ctx=ctx)

    countVar = 0
    ss = model.new_solution()
    nodes_ordered_k = []
    for k in K:
        #1. get nodes of vehicle k
        current = startDepot()
        nodes_V = [0]; nodes_P = []; nodes_D=[]
        while current != endDepot(len(V)):
            bool_break = True
            for i in V:
                if (current, i) not in Arcs:
                    if x_warm[current, i, k].solution_value == 1 and bool_break:
                        if i in P:
                            nodes_V.append(i)
                            nodes_P.append(i)
                            nodes_V.append(delivery(i))
                            nodes_D.append(delivery(i))
                        if i == endDepot(len(V)):
                            nodes_V.append(i)
                        current = i
                        bool_break = False


        #2. order them though running the TSP
        sol_info, x_tsp, t_tsp, q_tsp, \
        r_tsp, cpu_tsp, gap_tsp = tsp(nodes_V, nodes_P, nodes_D, ctx, useTW, False)
        current = 0
        nodes_ordered = [0]
        if sol_info != None :
            while current != endDepot(len(V)):
                bool_break = True
                for i in nodes_V:
                    if (current, i) not in Arcs:
                        if x_tsp[current, i, 0].solution_value == 1 and bool_break:

                            nodes_ordered.append(i)
                            current = i
                            bool_break = False

        current = 0
        #print(nodes_ordered)
        nodes_ordered_k.append(nodes_ordered)
        for i in nodes_ordered:
            if i != startDepot():
                countVar+=1
                ss.add_var_value(x[current, i, k], 1)
                current = i
    ss_best = ss
    #ss_best = greedyInsertion(x, ss, nodes_ordered_k, model, ctx)
    if countVar != 0:
        model.add_mip_start(ss_best)


def tsp(subV, subP, subD, ctx, useTW,logging_CPLEX):
    n = ctx.getNbVertices()
    Q = ctx.getCapacity()
    request = ctx.getRequest()
    MAX_RIDING = ctx.getMaxRiding()
    cost_time = ctx.getCost_time()
    cost_distance = ctx.getCost_distance()
    duration_stop = ctx.getDuration_stop()


    #same model as darp without the set K
    m = Model('tsp')
    Arcs = forbiddenArcs(subV, subP, subD, n, constaints=True, ctx=ctx)

    #variables and constraints
    x = m.binary_var_dict([(i, j, 0) for i in subV for j in subV if (i, j) not in Arcs], name='x')
    t = m.continuous_var_dict([i for i in subV], name='t')
    q = m.continuous_var_dict([i for i in subV], lb=0, ub=Q, name='q')
    r = m.continuous_var_dict([i for i in subP], lb=0, ub=MAX_RIDING, name='r')

    #constraints routing
    m.add_constraint(m.sum(x[startDepot(), i, 0] for i in subV if (startDepot(), i) not in Arcs) == 1)
    m.add_constraint(m.sum(x[i, endDepot(n), 0] for i in subV if (i, endDepot(n)) not in Arcs) == 1)
    m.add_constraints(m.sum(x[i, j, 0] for j in subV if (i, j) not in Arcs) == 1 for i in subP)
    m.add_constraints(m.sum(x[i, j, 0] for j in subV if (i, j) not in Arcs) -
                      m.sum(x[delivery(i), j, 0] for j in subV if (delivery(i), j) not in Arcs) == 0
                      for i in subP)
    m.add_constraints(m.sum(x[j, i, 0] for j in subV if (j, i) not in Arcs) -
                      m.sum(x[i, j, 0] for j in subV if (i, j) not in Arcs) == 0
                      for i in subP+subD)


    #constaints arrival times
    m.add_indicator_constraints(
        m.indicator_constraint(x[i, j, 0],  t[j] >= t[i] + duration_stop[i] + cost_time[i, j, 0])
        for i in subV for j in subV if i != j and (i, j) not in Arcs)

    m.add_constraints(t[i] + duration_stop[i] + cost_time[i, delivery(i), 0] <= t[delivery(i)]
                      for i in subP)

    #time-windows
    if useTW:
        m.add_constraints(ctx.getTw_start()[i] <= t[i] for i in subP+subD)
        m.add_constraints(t[i] <= ctx.getTw_end()[i] for i in subP+subD)

    #constraints capacity
    m.add_indicator_constraints(
        m.indicator_constraint(x[i, j, 0], q[j] >= q[i] + request[i])
        for i in subV for j in subV if i != j and (i, j) not in Arcs)

    #constraints riding times
    m.add_constraints(r[i] >= t[delivery(i)] - (t[i] - duration_stop[i]) for i in subP)
    m.add_constraints(r[i] >= cost_time[i, delivery(i), 0] for i in subP)

    #objective
    weights = ctx.getWeightsObj()
    emissions = ctx.getEmissionGHG()
    cost_obj = weights[0]*m.sum(cost_time[i, j, 0]*x[i, j, 0]
                                for i in subV for j in subV if (i, j) not in Arcs) + \
               weights[1]*m.sum(r[i] for i in subP)
    environ_obj = m.sum(emissions[0]*(cost_distance[i, j]/1000.0)*x[i, j, 0]
                        for i in subV for j in subV if (i, j) not in Arcs)
    m.minimize(cost_obj + weights[2]*environ_obj)

    # if len(couplesOptim) != 0:
    #     for cpl in couplesOptim:
    #         m.add_constraint(x[cpl[0], cpl[1]] == 1)


    #solve
    m.parameters.mip.tolerances.mipgap = 0.01
    m.set_time_limit(60)
    start = time.time()
    sol_info = m.solve(log_output=logging_CPLEX)
    cpu = time.time()-start
    gap = m.solve_details.mip_relative_gap
    return sol_info, x, t, q, r, cpu, gap


def darp(timelimit, MIPgap, warm, x_warm, logging_CPLEX, useTW, ctx):
    V = ctx.getV()
    P = ctx.getP()
    D = ctx.getD()
    K = ctx.getK()
    Q = ctx.getCapacity()
    n = ctx.getNbVertices()
    request = ctx.getRequest()
    MAX_RIDING = ctx.getMaxRiding()
    cost_time = ctx.getCost_time()
    cost_distance = ctx.getCost_distance()
    duration_stop = ctx.getDuration_stop()

    #1. Specify the model
    m = Model('darp')
    Arcs = forbiddenArcs(V, P, D, n, constaints= True, ctx=ctx)

    #variables and constraints
    x = m.binary_var_dict([(i, j, k) for i in V for j in V if (i, j) not in Arcs for k in K], name='x')
    t = m.continuous_var_dict([(i, k) for i in V for k in K], name='t')
    q = m.continuous_var_dict([(i, k) for i in V for k in K], lb=0, ub=Q, name='q')
    r = m.continuous_var_dict([i for i in P], lb=0, ub=MAX_RIDING, name='r')

    #constraints routing
    m.add_constraints(m.sum(x[startDepot(), i, k] for i in V if (startDepot(), i) not in Arcs) == 1
                      for k in K) # o -> i for each k
    m.add_constraints(m.sum(x[i, endDepot(n), k] for i in V if (i, endDepot(n)) not in Arcs) == 1
                      for k in K) # i -> d for each k
    m.add_constraints(m.sum(x[i, j, k] for j in V if (i, j) not in Arcs for k in K) == 1
                      for i in P)
    m.add_constraints(m.sum(x[i, j, k] for j in V if (i, j) not in Arcs) -
                      m.sum(x[delivery(i), j, k] for j in V if (delivery(i), j) not in Arcs) == 0
                      for i in P for k in K)
    m.add_constraints(m.sum(x[j, i, k] for j in V if (j, i) not in Arcs) -
                      m.sum(x[i, j, k] for j in V if (i, j) not in Arcs) == 0
                      for i in P+D for k in K)
    # if warm:
    # m.add_constraints(m.sum(x[i, j, k]*0.5 for i in V for j in V if (i, j) not in Arcs) >=
    #                   (len(V)-2)/len(K)-4 for k in K)

    #constaints arrival times
    m.add_indicator_constraints(
        m.indicator_constraint(x[i, j, k],  t[j, k] >= t[i, k] + duration_stop[i] + cost_time[i, j, 0])
        for i in V for j in V if i != j and (i, j) not in Arcs for k in K)
    if not warm:
        m.add_constraints(t[i, k] + duration_stop[i] + cost_time[i, delivery(i), 0] <= t[delivery(i), k]
                          for i in P for k in K)

    #time-windows
    if useTW:
        m.add_constraints(ctx.getTw_start()[i] <= t[i, k] for i in P+D for k in K)
        m.add_constraints(t[i, k] <= ctx.getTw_end()[i] for i in P+D for k in K)

    #constraints capacity
    m.add_indicator_constraints(
        m.indicator_constraint(x[i, j, k], q[j, k] >= q[i, k] + request[i])
        for i in V for j in V if i != j and (i, j) not in Arcs for k in K)
    #m.add_constraints(max(0, request[i]) <= q[i, k] for i in V for k in K)
    #m.add_constraints(q[i, k] <= min(Q, Q + request[i]) for i in V for k in K)

    #constraints riding times
    m.add_constraints(r[i] >= t[delivery(i), k] - (t[i, k] + duration_stop[i]) for i in P for k in K)
    m.add_constraints(r[i] >= cost_time[i, delivery(i), 0] for i in P)

    #1.2 Objectives
    weights = ctx.getWeightsObj()
    emissions = ctx.getEmissionGHG()
    cost_obj = weights[0]*m.sum(cost_time[i, j, 0]*x[i, j, k]
                                for i in V for j in V if (i, j) not in Arcs for k in K) + \
               weights[1]*m.sum(r[i] for i in P)
    environ_obj = m.sum(emissions[0]*(cost_distance[i, j]/1000.0)*x[i, j, k]
                        for i in V for j in V if (i, j) not in Arcs for k in K)

    # if len(couplesOptim) != 0:
    #     for cpl in couplesOptim:
    #         print(cpl[0], cpl[1])
    #         m.add_constraint(m.sum(x[cpl[0], cpl[1], k] for k in K) == 1)

    #m.minimize_static_lex([cost_obj, environ_obj])
    # sense="min"
    # exprs=[cost_obj, environ_obj]
    # priorities=[1, 2]
    # weights=[1, 0.25]
    # m.set_multi_objective(sense, exprs, priorities, weights, abstols=None, reltols=None, names=None)

    #case of one objective
    m.minimize(cost_obj + weights[2]*environ_obj)

    if not warm:
        setStartSol(x, x_warm, m, set, ctx)

    #m.export_as_lp("run/Feyzin_darp.lp")
    if logging_CPLEX: m.print_information(); print("")

    #Solve solve !!
    #m.parameters.threads = 2
    #m.parameters.timelimit = 600
    m.parameters.mip.tolerances.mipgap = MIPgap
    #if warm:
    m.set_time_limit(timelimit)

    start = time.time()
    sol_info = m.solve(log_output=logging_CPLEX)
    cpu =  time.time()-start
    gap = m.solve_details.mip_relative_gap
    if not warm and logging_CPLEX:
        print("CPU time= %.3f" %cpu, " MIP gap= %.3f" %gap)
    return sol_info, x, t, q, r, cpu, gap



def darp_contacted(ctx_contacted, maptoVinit, nbV, cap, maxR, method, useTW, logging_CPLEX):
    ctx_contacted.setNbVehicles(nbV)
    ctx_contacted.setCapacity(cap)
    ctx_contacted.setMaxRiding(maxR)

    if nbV == 1:
        sol_info, x, t, q, r, cpu, gap = tsp(ctx_contacted.V, ctx_contacted.P,
                                             ctx_contacted.D, ctx_contacted,
                                             useTW, logging_CPLEX)
    else:
       # sol_info_w, x_w, t_w, q_w, r_w, cpu_w, gap_w = darp(timelimit=60*5, MIPgap=0.01, warm=True,
                                                            #x_warm=None, logging_CPLEX=False,
                                                            #useTW=useTW,  ctx=ctx_contacted)

        sol_info, x, t, q, r, cpu, gap = darp(timelimit=60*20, MIPgap=0.01, warm=True, x_warm=None,
                                              logging_CPLEX=logging_CPLEX, useTW=useTW,
                                              ctx=ctx_contacted)
    VUR, ZOR = VUR_ZOR_actual(x, maptoVinit, ctx_contacted, ctx_init)
    print(method, ", %.3f" % cpu, ", %.3f" % gap, ",",rC, ",", nbV, ",", cap, ",",
          maxR, ",%.3f" % VKT(x, ctx_contacted), ",%.3f" % VUR,
          ",%.3f" % ZOR, ",%.3f" % GHG(x, ctx_contacted))
    #print(sol_info.solve_status, sol_info)
    #plotRoutes(method, x, ctx_contacted)

#
# Indicators
#

def VOR(q, x, ctx):
    V = ctx.getV()
    K = ctx.getK()
    P = ctx.getP()
    D = ctx.getD()
    n = len(V)
    Q = ctx.getCapacity()
    #Arcs = forbiddenArcs(V, P, D, n)
    Arcs = forbiddenArcs(V, P, D, n, constaints= True, ctx=ctx)
    listQ = [q[i, k].solution_value for i in V for j in V for k in K
             if (j, i) not in Arcs and x[j, i, k].solution_value == 1]
    return sum(listQ)/(Q*len(listQ))


def VUR_ZOR_actual(x, maptoVinit, ctx, ctx_init):
    V_init =  ctx_init.getV()

    V = ctx.getV()
    K = ctx.getK()
    P = ctx.getP()
    D = ctx.getD()
    Q = ctx.getCapacity()
    request_init = ctx_init.getRequest()
    n = len(V)

    #Arcs = forbiddenArcs(V, P, D, n)
    Arcs = forbiddenArcs(V, P, D, n, constaints= True, ctx=ctx)
    rate = 0.0
    count = 0
    countZeroOccupancy = 0
    for k in K:
        current = 0
        nodes = []

        while current != endDepot(n):
            bool_break = True
            for i in V:
                if bool_break:
                    if (current, i) not in Arcs:
                        if x[current, i, k].solution_value == 1:
                            if isinstance(maptoVinit[i], int):
                                nodes.append(maptoVinit[i])
                            else:
                                nodes.append(maptoVinit[i][0])
                                nodes.append(maptoVinit[i][1])
                            current = i
                            bool_break = False
        current_cap = 0.0
        for i in nodes:
            if i != startDepot() and i != endDepot(len(V_init)):
                if current_cap == 0.0:
                    countZeroOccupancy += 1
                current_cap += request_init[i]
                rate += current_cap/(Q*(len(nodes)-2))
                count += 1
    VUR = rate/len(K); ZOR= countZeroOccupancy/(count*len(K))
    return VUR, ZOR


def VKT(x, ctx):
    V = ctx.getV()
    K = ctx.getK()
    P = ctx.getP()
    D = ctx.getD()
    n = len(V)
    cost_distance = ctx.getCost_distance()
    #Arcs = forbiddenArcs(V, P, D, n)
    Arcs = forbiddenArcs(V, P, D, n, constaints= True, ctx=ctx)
    return sum([cost_distance[i, j]*x[i, j, k].solution_value/1000.0 for i in V
                for j in V if (i, j) not in Arcs for k in K])


def GHG(x, ctx):
    V = ctx.getV()
    K = ctx.getK()
    P = ctx.getP()
    D = ctx.getD()
    emissions = ctx.getEmissionGHG()
    cost_distance = ctx.getCost_distance()
    n = len(V)
    #Arcs = forbiddenArcs(V, P, D, n)
    Arcs = forbiddenArcs(V, P, D, n, constaints= True, ctx=ctx)
    return sum([cost_distance[i, j]*x[i, j, k].solution_value*emissions[0]/1000 for i in V
                for j in V if (i, j) not in Arcs for k in K])


#
# Run Run ...
#
if __name__ == "__main__":
    #I.Data
    #sys.stdout = open("log.txt", "w")
    durationStop = 60 #seconds
    ctx_init = readDataGuy(filenameTime="data/FeyzinTime.csv",
                           filenameDistance="data/FeyzinDistance.csv",
                           filenameDemande="data/FeyzinDemande.csv",
                           durationStop=durationStop)
    ctx_init.setEmissionGHG([0.171, 0.05])#Kg CO2-eq/km ... 1st for vehicle from Gawron et al. (2019), 2nd for passenger
    ctx_init.setWeightsObj([1.0, 1.0, 0.1]) #first 2 weights for cost objective, (the seconds 2 for envirnmental obj)
    G_init = graphRouting(ctx_init)
    plotMapwithStations()
   
    

    # #II. Optimization
    logging = False
    loggingCPLEX = False

    rC = 0.5  #ratio de contraction 
    timesC = 1
    useTW = True
    nTW = 2
    lengthTW = 3600

    # maxR=11*60 #FeyzinMin
    maxR=15*60 #Feyzin


    # Look for the best 




    print("method,cpu,gap,rateContrac,nbV,cap,MaxR,VKT,VUR,ZOR,GHG")
    # baseline scenario
    
    nbV=4; cap=25
    ctx_contacted0, maptoVinit0 = contractN2V(G_init, ctx_init, rC, durationStop, cap, maxR, timesC, logging)
    ctx_contacted1, maptoVinit1 = contractBasic(G_init, ctx_init, rC, durationStop, cap, maxR, logging)
    ctx_contacted2, maptoVinit2 = contractKmeans(G_init, ctx_init, rC, durationStop, cap, maxR, timesC, logging)
    ctx_contacted3, maptoVinit3 = contractRequest(G_init, ctx_init, rC, durationStop, cap, maxR, timesC, logging)
    setTW(nTW, lengthTW, ctx_contacted0, ctx_contacted1, ctx_contacted2, ctx_contacted3)
    darp_contacted(ctx_contacted0, maptoVinit0, nbV, cap, maxR, "N2V", useTW, logging)
    darp_contacted(ctx_contacted1, maptoVinit1, nbV, cap, maxR, "Basic", useTW, logging)
    darp_contacted(ctx_contacted2, maptoVinit2, nbV, cap, maxR, "Kmeans", useTW, logging)
    darp_contacted(ctx_contacted3, maptoVinit3, nbV, cap, maxR, "Request", useTW, logging)




    #sensitivity nbV
    # cap=25
    # ctx_contacted0, maptoVinit0 = contractN2V(G_init, ctx_init, rC, durationStop, cap, maxR, timesC, logging)
    # ctx_contacted1, maptoVinit1 = contractBasic(G_init, ctx_init, rC, durationStop, cap, maxR, logging)
    # ctx_contacted2, maptoVinit2 = contractKmeans(G_init, ctx_init, rC, durationStop, cap, maxR, timesC, logging)
    # ctx_contacted3, maptoVinit3 = contractRequest(G_init, ctx_init, rC, durationStop, cap, maxR, timesC, logging)
    # setTW(nTW, lengthTW, ctx_contacted0, ctx_contacted1, ctx_contacted2, ctx_contacted3)
    # for nbV in range(3, 11):
    #     time.sleep(60)
    #     darp_contacted(ctx_contacted0, maptoVinit0, nbV, cap, maxR, "N2V", useTW, loggingCPLEX)
    #     time.sleep(60)
    #     darp_contacted(ctx_contacted1, maptoVinit1, nbV, cap, maxR, "Basic", useTW, loggingCPLEX)
    #     time.sleep(60)
    #     darp_contacted(ctx_contacted2, maptoVinit2, nbV, cap, maxR, "Kmeans", useTW, loggingCPLEX)
    #     time.sleep(60)
    #     darp_contacted(ctx_contacted3, maptoVinit3, nbV, cap, maxR, "Request", useTW, loggingCPLEX)
        


    # sensitivity cap
    nbV = 4
    for cap in 30, 35, 40, 45, 50: #20, 25,
        ctx_contacted0, maptoVinit0 = contractN2V(G_init, ctx_init, rC, durationStop, cap, maxR, timesC, logging)
        ctx_contacted1, maptoVinit1 = contractBasic(G_init, ctx_init, rC, durationStop, cap, maxR, logging)
        ctx_contacted2, maptoVinit2 = contractKmeans(G_init, ctx_init, rC, durationStop, cap, maxR, timesC, logging)
        ctx_contacted3, maptoVinit3 = contractRequest(G_init, ctx_init, rC, durationStop, cap, maxR, timesC, logging)
        
        #darp_contacted(ctx_contacted0, maptoVinit0, nbV, cap, maxR, "N2V", useTW, loggingCPLEX)
        #darp_contacted(ctx_contacted1, maptoVinit1, nbV, cap, maxR, "Basic", useTW, loggingCPLEX)
        #darp_contacted(ctx_contacted2, maptoVinit2, nbV, cap, maxR, "Kmeans", useTW, loggingCPLEX)
        darp_contacted(ctx_contacted3, maptoVinit3, nbV, cap, maxR, "Request", useTW, loggingCPLEX)

    # ALL PARAMETERS
    # for maxR in 15*60, 16*60, 17*60, 18*60, 19*60, 20*60: # 10*60, 11*60, 12*60, 13*60, 14*60,
    #     for cap in 30, 35, 40, 45, 50: #20, 25,
    #         ctx_contacted0, maptoVinit0 = contractN2V(G_init, ctx_init, rC, durationStop, cap, maxR, timesC, logging)
    #         ctx_contacted1, maptoVinit1 = contractBasic(G_init, ctx_init, rC, durationStop, cap, maxR, logging)
    #         ctx_contacted2, maptoVinit2 = contractKmeans(G_init, ctx_init, rC, durationStop, cap, maxR, timesC, logging)
    #         ctx_contacted3, maptoVinit3 = contractRequest(G_init, ctx_init, rC, durationStop, cap, maxR, timesC, logging)
    #         setTW(nTW, lengthTW, ctx_contacted0, ctx_contacted1, ctx_contacted2, ctx_contacted3)

    #         for nbV in range(3, 11):
    #             darp_contacted(ctx_contacted0, maptoVinit0, nbV, cap, maxR, "N2V", useTW, loggingCPLEX)
    #             darp_contacted(ctx_contacted1, maptoVinit1, nbV, cap, maxR, "Basic", useTW, loggingCPLEX)
    #             darp_contacted(ctx_contacted2, maptoVinit2, nbV, cap, maxR, "Kmeans", useTW, loggingCPLEX)
    #             darp_contacted(ctx_contacted3, maptoVinit3, nbV, cap, maxR, "Request", useTW, loggingCPLEX)
                

    #sensitivity maxR
# =============================================================================
#     nbV=4; cap=25
#     for maxR in 10*60, 11*60, 12*60, 13*60, 14*60, 15*60, 16*60, 17*60, 18*60, 19*60, 20*60: #
#         ctx_contacted, maptoVinit = contractN2V(G_init, ctx_init, rC, durationStop, cap, maxR, timesC, False)
#         ctx_contacted1, maptoVinit1 = contractBasic(G_init, ctx_init, rC, durationStop, cap, maxR, False)
#         ctx_contacted2, maptoVinit2 = contractKmeans(G_init, ctx_init, rC, durationStop, cap, maxR, timesC, False)
#         setTW(nTW, lengthTW, ctx_contacted2, ctx_contacted1, ctx_contacted2)
# 
# =============================================================================
        #for i in ctx_contacted.P: print("p -> d(N2V)", ctx_contacted.getCost_time()[i, delivery(i),0])
        #for i in ctx_contacted1.P: print("p -> d(B)", ctx_contacted1.getCost_time()[i, delivery(i),0])
        #for i in ctx_contacted2.P: print("p -> d(Clus)", ctx_contacted2.getCost_time()[i, delivery(i),0])
        #print("len(V)-N2V", len(ctx_contacted.V), "len(V)-B", len(ctx_contacted1.V), "len(V)-Clus", len(ctx_contacted2.V))

        #time.sleep(30)
        # darp_contacted(ctx_contacted, maptoVinit, nbV, cap, maxR, "N2V", useTW, False)
        #time.sleep(30)
        #darp_contacted(ctx_contacted2, maptoVinit2, nbV, cap, maxR, "Kmeans", useTW, False)
        #darp_contacted(ctx_contacted1, maptoVinit1, nbV, cap, maxR, "Basic", useTW, False)




# def VKT_baseG(x, ctx, ctx_init):
#     #Not used
#     V_init =  ctx_init.getV()
#     cost_distance = ctx_init.getCost_distance()
#
#
#     V = ctx.getV()
#     K = ctx.getK()
#     P = ctx.getP()
#     D = ctx.getD()
#     n = len(V)
#     Arcs = forbiddenArcs(V, P, D, n)
#
#     sum=0.0
#     for k in K:
#         current = 0
#         nodes = []
#         while current != endDepot(len(V)):
#             bool_break = True
#             for i in V:
#                 if (current, i) not in Arcs:
#                     if x[current, i, k].solution_value == 1 and bool_break:
#                         if isinstance(mappingVinit[i], int):
#                             nodes.append(mappingVinit[i])
#                         else:
#                             nodes.append(mappingVinit[i][0])
#                             nodes.append(mappingVinit[i][1])
#                         current = i
#                         bool_break = False
#         current = 0
#         for i in nodes:
#             sum +=cost_distance[current, i]
#         sum +=cost_distance[current, endDepot(len(V_init))]
#     return sum/1000.0
