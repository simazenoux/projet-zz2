import matplotlib.pyplot as plt
import numpy as np

#Old plots

#
#1. sensitivity to nbV
#

#FeyzinMin
# nbV=range(1,7)
# N2V_VKT_nbV=[31.138, 30.175, 29.001, 27.827, 26.653, 25.479]
# N2V_VUR_nbV=[0.289, 0.302, 0.300, 0.351, 0.339, 0.369]
# N2V_VUR_nbV = [x * 100 for x in N2V_VUR_nbV]
# N2V_ZOR_nbV=[0.333, 0.167, 0.111, 0.083, 0.067, 0.056]
# N2V_ZOR_nbV = [x * 100 for x in N2V_ZOR_nbV]
# N2V_GHG_nbV=[5.325, 5.160, 4.959, 4.758, 4.558, 4.357]
#
# Kmeans_VKT_nbV=[34.783, 32.936, 33.978, 32.804, 31.630, 30.456]
# Kmeans_VUR_nbV=[0.271, 0.305, 0.294, 0.287, 0.312, 0.347]
# Kmeans_VUR_nbV = [x * 100 for x in Kmeans_VUR_nbV]
# Kmeans_ZOR_nbV=[0.375, 0.188, 0.125, 0.094, 0.075, 0.062]
# Kmeans_ZOR_nbV = [x * 100 for x in Kmeans_ZOR_nbV]
# Kmeans_GHG_nbV=[5.948, 5.632, 5.810, 5.609, 5.409, 5.208]




nbV=range(1, 11)
N2V_VKT_nbV=[67.697, 67.043, 65.653, 65.466, 64.530, 62.791, 61.617, 60.443, 59.269, 59.884]
N2V_VUR_nbV=[0.308, 0.417, 0.322, 0.421, 0.435, 0.424, 0.425, 0.398, 0.386, 0.394]
N2V_VUR_nbV = [x * 100 for x in N2V_VUR_nbV]
N2V_ZOR_nbV=[0.352, 0.176, 0.117, 0.088, 0.070, 0.059, 0.050, 0.044, 0.039, 0.035]
N2V_ZOR_nbV = [x * 100 for x in N2V_ZOR_nbV]
N2V_GHG_nbV=[11.576, 11.464, 11.227, 11.195, 11.035, 10.737, 10.537, 10.336, 10.135, 10.240]
N2V_GHG_nbV = [(43.8-x)/43.8 * 100 for x in N2V_GHG_nbV]

Kmeans_VKT_nbV=[78.683, 76.794, 75.831, 74.441, 73.028, 73.859, 73.754, 72.580, 72.580, 73.172]
Kmeans_VUR_nbV=[0.294, 0.299, 0.369, 0.316, 0.294, 0.369, 0.387, 0.354, 0.382, 0.381]
Kmeans_VUR_nbV = [x * 100 for x in Kmeans_VUR_nbV]
Kmeans_ZOR_nbV=[0.389, 0.194, 0.130, 0.097, 0.078, 0.065, 0.056, 0.049, 0.043, 0.039]
Kmeans_ZOR_nbV = [x * 100 for x in Kmeans_ZOR_nbV]
Kmeans_GHG_nbV=[13.455, 13.132, 12.967, 12.729, 12.488, 12.630, 12.612, 12.411, 12.411, 12.512]
Kmeans_GHG_nbV = [(43.8-x)/43.8 * 100 for x in Kmeans_GHG_nbV]

#Fig1
fig = plt.figure()
ax = plt.axes()
plt.plot(nbV, N2V_VKT_nbV, 'ro-', c='r', alpha=0.7, label='N2V')
plt.plot(nbV, Kmeans_VKT_nbV, 'ro-', c='b', alpha=0.7, label='K-means')
plt.title('VKT in function of |K|', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_xticks(nbV)
ax.set_ylim([50, 82])
ax.legend()
plt.xlabel('vehicles number |K|', fontsize=14)
plt.ylabel('VKT in kilometers', fontsize=14)
#plt.show()
fig.savefig('run/nbV-VKT.png')

#Fig2
fig = plt.figure()
ax = plt.axes()
plt.plot(nbV, N2V_VUR_nbV, 'ro-', c='r', alpha=0.7, label='N2V')
plt.plot(nbV, Kmeans_VUR_nbV, 'ro-', c='b', alpha=0.7, label='K-means')
plt.title('VOR in function of |K|', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_xticks(nbV)
ax.set_ylim([0,60])
ax.legend()
plt.xlabel('vehicles number |K|', fontsize=14)
plt.ylabel('VOR [%]', fontsize=14)
#plt.show()
fig.savefig('run/nbV-VOR.png')


#Fig3
fig = plt.figure()
ax = plt.axes()
plt.plot(nbV, N2V_ZOR_nbV, 'ro-', c='r', alpha=0.7, label='N2V')
plt.plot(nbV, Kmeans_ZOR_nbV, 'ro-', c='b', alpha=0.7, label='K-means')
plt.title('ZOR in function of |K|', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_xticks(nbV)
ax.set_ylim([0, 50])
ax.legend()
plt.xlabel('vehicles number |K|', fontsize=14)
plt.ylabel('ZOR [%]', fontsize=14)
#plt.show()
fig.savefig('run/nbV-ZOR.png')

#Fig4
fig = plt.figure()
ax = plt.axes()
plt.plot(nbV, N2V_GHG_nbV, 'ro-', c='r', alpha=0.7, label='N2V')
plt.plot(nbV, Kmeans_GHG_nbV, 'ro-', c='b', alpha=0.7, label='K-means')
plt.title('GHG gain in function of |K|', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_xticks(nbV)
ax.set_ylim([60, 90])
ax.legend()
plt.xlabel('vehicles number |K|', fontsize=14)
plt.ylabel('GHG gain [%]', fontsize=14)
#plt.show()
fig.savefig('run/nbV-GHG.png')


#
#3. sensitivity to capacity
#

#FeyzinMin
# cap=[20, 25, 30, 35, 40, 45, 50]
# N2V_VKT_cap=[29.001, 29.001, 29.406, 29.001, 29.406, 27.029, 27.029]
# N2V_VUR_cap=[0.376, 0.300, 0.241, 0.196, 0.181, 0.254, 0.260]
# N2V_VUR_cap = [x * 100 for x in N2V_VUR_cap]
# N2V_ZOR_cap=[0.111, 0.111, 0.097, 0.111, 0.097, 0.069, 0.069]
# N2V_ZOR_cap = [x * 100 for x in N2V_ZOR_cap]
# N2V_GHG_cap=[4.959, 4.959, 5.028, 4.959, 5.028, 4.622, 4.622]
#
# Kmeans_VKT_cap=[33.978, 33.978, 32.077, 32.077, 32.077, 27.777, 27.777]
# Kmeans_VUR_cap=[0.462, 0.294, 0.457, 0.392, 0.343, 0.211, 0.256]
# Kmeans_VUR_cap = [x * 100 for x in Kmeans_VUR_cap]
# Kmeans_ZOR_cap=[0.125, 0.125, 0.097, 0.097, 0.097, 0.083, 0.083]
# Kmeans_ZOR_cap = [x * 100 for x in Kmeans_ZOR_cap]
# Kmeans_GHG_cap=[5.810, 5.810, 5.485, 5.485, 5.485, 4.750, 4.750]


#Feyzin
cap=[20, 25, 30, 35, 40, 45, 50]
N2V_VKT_cap=[70.877, 68.167, 63.072, 62.429, 62.495, 54.126, 51.640]
N2V_VUR_cap=[0.385, 0.324, 0.405, 0.340, 0.281, 0.336, 0.249]
N2V_VUR_cap = [x * 100 for x in N2V_VUR_cap]
N2V_ZOR_cap=[0.097, 0.097, 0.074, 0.074, 0.083, 0.069, 0.065]
N2V_ZOR_cap = [x * 100 for x in N2V_ZOR_cap]
N2V_GHG_cap=[12.120, 11.657, 10.785, 10.675, 10.687, 9.256, 8.830]
N2V_GHG_cap = [(43.8-x)/43.8 * 100 for x in N2V_GHG_cap]

Kmeans_VKT_cap=[70.144, 73.286, 70.776, 80.066, 78.336, 70.620, 66.255]
Kmeans_VUR_cap=[0.433, 0.382, 0.407, 0.248, 0.240, 0.243, 0.227]
Kmeans_VUR_cap = [x * 100 for x in Kmeans_VUR_cap]
Kmeans_ZOR_cap=[0.097, 0.093, 0.083, 0.093, 0.093, 0.074, 0.074]
Kmeans_ZOR_cap = [x * 100 for x in Kmeans_ZOR_cap]
Kmeans_GHG_cap=[11.995, 12.532, 12.103, 13.691, 13.395, 12.076, 11.330]
Kmeans_GHG_cap = [(43.8-x)/43.8 * 100 for x in Kmeans_GHG_cap]


#Fig5
fig = plt.figure()
ax = plt.axes()
plt.plot(cap, N2V_VKT_cap, 'ro-', c='r', alpha=0.7, label='N2V')
plt.plot(cap, Kmeans_VKT_cap, 'ro-', c='b', alpha=0.7, label='K-means')
plt.title('VKT in function of Q', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_xticks(cap)
ax.set_ylim([50, 82])
ax.legend()
plt.xlabel('Vehicle capacity Q', fontsize=14)
plt.ylabel('VKT in kilometers', fontsize=14)
#plt.show()
fig.savefig('run/cap-VKT.png')

#Fig6
fig = plt.figure()
ax = plt.axes()
plt.plot(cap, N2V_VUR_cap, 'ro-', c='r', alpha=0.7, label='N2V')
plt.plot(cap, Kmeans_VUR_cap, 'ro-', c='b', alpha=0.7, label='K-means')
plt.title('VOR in function of Q', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_xticks(cap)
ax.set_ylim([0,60])
ax.legend()
plt.xlabel('Vehicle capacity Q', fontsize=14)
plt.ylabel('VOR [%]', fontsize=14)
#plt.show()
fig.savefig('run/cap-VOR.png')


#Fig7
fig = plt.figure()
ax = plt.axes()
plt.plot(cap, N2V_ZOR_cap, 'ro-', c='r', alpha=0.7, label='N2V')
plt.plot(cap, Kmeans_ZOR_cap, 'ro-', c='b', alpha=0.7, label='K-means')
plt.title('ZOR in function of Q', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_xticks(cap)
ax.set_ylim([0, 50])
ax.legend()
plt.xlabel('Vehicle capacity Q', fontsize=14)
plt.ylabel('ZOR [%]', fontsize=14)
#plt.show()
fig.savefig('run/cap-ZOR.png')

#Fig8
fig = plt.figure()
ax = plt.axes()
plt.plot(cap, N2V_GHG_cap, 'ro-', c='r', alpha=0.7, label='N2V')
plt.plot(cap, Kmeans_GHG_cap, 'ro-', c='b', alpha=0.7, label='K-means')
plt.title('GHG gain in function of Q', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_xticks(cap)
ax.set_ylim([60, 90])
ax.legend()
plt.xlabel('Vehicle capacity Q', fontsize=14)
plt.ylabel('GHG gain [%]', fontsize=14)
#plt.show()
fig.savefig('run/cap-GHG.png')


#
#2. sensitivity to maxR
#

#FeyzinMin
# maxR=[7, 7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12]
# N2V_VKT_maxR=[27.783, 27.783, 29.001, 29.001, 29.001, 29.001, 29.001, 29.001, 29.001, 29.001, 29.001]
# N2V_VUR_maxR=[0.427, 0.387, 0.361, 0.339, 0.364, 0.339, 0.300, 0.300, 0.300, 0.300, 0.313]
# N2V_VUR_maxR = [x * 100 for x in N2V_VUR_maxR]
# N2V_ZOR_maxR=[0.097, 0.097, 0.111, 0.111, 0.111, 0.111, 0.111, 0.111, 0.111, 0.111, 0.111]
# N2V_ZOR_maxR = [x * 100 for x in N2V_ZOR_maxR]
# N2V_GHG_maxR=[4.751, 4.751, 4.959, 4.959, 4.959, 4.959, 4.959, 4.959, 4.959, 4.959, 4.959]
#
# Kmeans_VKT_maxR=[27.783, 27.783, 32.513, 33.978, 33.978, 33.978, 33.978, 33.978, 33.978, 33.978, 33.978]
# Kmeans_VUR_maxR=[0.427, 0.366, 0.401, 0.370, 0.294, 0.294, 0.294, 0.294, 0.294, 0.294, 0.294]
# Kmeans_VUR_maxR = [x * 100 for x in Kmeans_VUR_maxR]
# Kmeans_ZOR_maxR=[0.097, 0.097, 0.111, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125]
# Kmeans_ZOR_maxR = [x * 100 for x in Kmeans_ZOR_maxR]
# Kmeans_GHG_maxR=[4.751, 4.751, 5.560, 5.810, 5.810, 5.810, 5.810, 5.810, 5.810, 5.810, 5.810]


#Feyzin
maxR=[10, 11, 12,  13, 14, 15, 16, 17, 18, 19, 20]
N2V_VKT_maxR=[64.355, 66.142, 65.393, 69.737, 66.014, 69.852, 67.377, 67.530, 71.874, 71.773, 68.118]
N2V_VUR_maxR=[0.303, 0.386, 0.349, 0.365, 0.364, 0.355, 0.328, 0.344, 0.336, 0.387, 0.375]
N2V_VUR_maxR = [x * 100 for x in N2V_VUR_maxR]
N2V_ZOR_maxR=[0.097, 0.097, 0.093, 0.097, 0.088, 0.093, 0.088, 0.097, 0.102, 0.097, 0.093]
N2V_ZOR_maxR = [x * 100 for x in N2V_ZOR_maxR]
N2V_GHG_maxR=[11.005, 11.310, 11.182, 11.925, 11.288, 11.945, 11.521, 11.548, 12.290, 12.273, 11.648]
N2V_GHG_maxR = [(43.8-x)/43.8 * 100 for x in N2V_GHG_maxR]

Kmeans_VKT_maxR=[72.023, 70.974, 68.507, 73.157, 74.202, 75.792, 74.878, 71.598, 76.639, 74.965, 73.157]
Kmeans_VUR_maxR=[0.317, 0.338, 0.404, 0.422, 0.324, 0.301, 0.419, 0.324, 0.296, 0.276, 0.378]
Kmeans_VUR_maxR = [x * 100 for x in Kmeans_VUR_maxR]
Kmeans_ZOR_maxR=[0.097, 0.097, 0.093, 0.093, 0.102, 0.102, 0.093, 0.102, 0.102, 0.102, 0.093]
Kmeans_ZOR_maxR = [x * 100 for x in Kmeans_ZOR_maxR]
Kmeans_GHG_maxR=[12.316, 12.137, 11.715, 12.510, 12.689, 12.960, 12.804, 12.243, 13.105, 12.819, 12.510]
Kmeans_GHG_maxR = [(43.8-x)/43.8 * 100 for x in Kmeans_GHG_maxR]

#Fig9
fig = plt.figure()
ax = plt.axes()
plt.plot(maxR, N2V_VKT_maxR, 'ro-', c='r', alpha=0.7, label='N2V')
plt.plot(maxR, Kmeans_VKT_maxR, 'ro-', c='b', alpha=0.7, label='K-means')
plt.title('VKT in function of L', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_xticks(maxR)
ax.set_ylim([50, 82])
ax.legend()
plt.xlabel('Maximum riding time L in minutes', fontsize=14)
plt.ylabel('VKT in kilometers', fontsize=14)
#plt.show()
fig.savefig('run/maxR-VKT.png')

#Fig10
fig = plt.figure()
ax = plt.axes()
plt.plot(maxR, N2V_VUR_maxR, 'ro-', c='r', alpha=0.7, label='N2V')
plt.plot(maxR, Kmeans_VUR_maxR, 'ro-', c='b', alpha=0.7, label='K-means')
plt.title('VOR in function of L', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_xticks(maxR)
ax.set_ylim([0,60])
ax.legend()
plt.xlabel('Maximum riding time L in minutes', fontsize=14)
plt.ylabel('VOR [%]', fontsize=14)
#plt.show()
fig.savefig('run/maxR-VOR.png')


#Fig11
fig = plt.figure()
ax = plt.axes()
plt.plot(maxR, N2V_ZOR_maxR, 'ro-', c='r', alpha=0.7, label='N2V')
plt.plot(maxR, Kmeans_ZOR_maxR, 'ro-', c='b', alpha=0.7, label='K-means')
plt.title('ZOR in function of L', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_xticks(maxR)
ax.set_ylim([0, 50])
ax.legend()
plt.xlabel('Maximum riding time L in minutes', fontsize=14)
plt.ylabel('ZOR [%]', fontsize=14)
#plt.show()
fig.savefig('run/maxR-ZOR.png')

#Fig12
fig = plt.figure()
ax = plt.axes()
plt.plot(maxR, N2V_GHG_maxR, 'ro-', c='r', alpha=0.7, label='N2V')
plt.plot(maxR, Kmeans_GHG_maxR, 'ro-', c='b', alpha=0.7, label='K-means')
plt.title('GHG gain in function of L', fontsize=20)
plt.xticks(size = 14)
plt.yticks(size = 14)
ax.set_xticks(maxR)
ax.set_ylim([60, 90])
ax.legend()
plt.xlabel('Maximum riding time L in minutes', fontsize=14)
plt.ylabel('GHG gain [%]', fontsize=14)
#plt.show()
fig.savefig('run/maxR-GHG.png')
